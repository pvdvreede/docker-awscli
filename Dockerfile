FROM debian:wheezy
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && apt-get install -y python-pip && apt-get clean
RUN pip install awscli
VOLUME ["/root/.aws"]

